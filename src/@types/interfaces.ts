export interface MessageRequest {
  context: "suitability";
  id: string | "final" | null;
  answers: QuestionAnswers;
}

export interface QuestionAnswers {
  [key: string]: string | number;
}

export interface FinishResponse {
  accessToken: string;
}

export interface ProfileResponse {
  title: string;
  headline: string;
  coverPictureURL: {
    full: string;
    regular: string;
  };
  sections: [
    {
      type: string;
      title: string;
      content: [
        {
          type: string;
          content: string;
        }
      ];
    }
  ];
}

export interface Responce {
  id: string | "final" | null;
  messages: MessageType[];
  buttons: UserResponseType[];
  inputs: InputType[];
  rows: UserResponseType[];
  radios: UserResponseType[];
  checkbox: UserResponseType[];
  responses: string[];
}

export interface InputType {
  type: string;
  mask: string;
}

export interface MessageType {
  type: string;
  value: string;
}

export interface UserResponseType {
  value: string;
  label: { title: string };
}
