import React from "react";
import s from "./style.scss";

interface ButtonProps {
  label: string;
  onClick(): void;
}

export const Button = ({ label, onClick }: ButtonProps) => {
  return (
    <section className={s.radio}>
      <input type="radio" className={s.responsiveButton} onClick={onClick} />
      <label>{label}</label>
    </section>
  );
};
