import React from "react";
import { FiSettings } from "react-icons/fi";
import s from "./style.scss";

interface ButtonProps {
  label: string;
  onClick(): void;
  isLoading?: boolean;
}

export const Button = ({ label, onClick, isLoading }: ButtonProps) => {
  return (
    <button
      className={isLoading ? s.buttonDisabled : s.responsiveButton}
      onClick={() => {
        if (!isLoading) {
          onClick();
        }
      }}
    >
      {label}
    </button>
  );
};
