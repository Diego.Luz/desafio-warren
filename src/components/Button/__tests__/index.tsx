import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render, fireEvent } from "@testing-library/react";
import { Button } from "..";

describe("<Button /> test case", () => {
  test("test if is in the document", () => {
    const { container } = render(
      <Button label="Click me" onClick={() => console.log("Clicked")} />
    );
    expect(container).toMatchSnapshot();
  });

  test("test click event", () => {
    const onClick = jest.fn();

    const { getByText } = render(<Button label="Click me" onClick={onClick} />);

    fireEvent.click(getByText("Click me"));

    expect(onClick).toHaveBeenCalled();
  });
});
