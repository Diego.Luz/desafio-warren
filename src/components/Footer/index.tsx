import React from "react";
import s from "./style.scss";

interface FooterProps {
  children: React.ReactNode;
  isButton: boolean;
  isLoading: boolean;
}

export const Footer = ({ children, isButton, isLoading }: FooterProps) => {
  return (
    <footer className={isLoading ? s.footerLoading : s.footer}>
      <div className={isButton ? s.buttonContent : s.inputContent}>
        {children}
      </div>
    </footer>
  );
};
