import React from "react";
import { ArrowLeft } from "../Icons/ArrowLeft/arrowLeft";
import s from "./style.scss";

interface HeaderProps {
  goBack: () => void;
  isProfile: boolean;
}

export const Header = ({ goBack, isProfile }: HeaderProps) => {
  return (
    <header className={s.header}>
      <nav className={s.nav}>
        {isProfile ? (
          <span>Perfil de Investidor</span>
        ) : (
          <>
            <ArrowLeft onClick={goBack} />
            <span>Descobrindo seu perfil</span>
          </>
        )}
      </nav>
    </header>
  );
};
