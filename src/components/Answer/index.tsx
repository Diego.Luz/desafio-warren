import React from "react";
import s from "./style.scss";

interface AnswerProps {
  text: string;
  userFirstLetter: string;
}

export const Answer = ({ text, userFirstLetter }: AnswerProps) => {
  return (
    <section className={s.message}>
      <p>{text}</p>
      <span>{userFirstLetter}</span>
    </section>
  );
};
