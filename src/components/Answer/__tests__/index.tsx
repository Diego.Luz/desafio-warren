import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render } from "@testing-library/react";
import { Answer } from "..";

describe("<Answer /> test case", () => {
  test("<Answer /> is in the document", () => {
    const { container } = render(<Answer text="Warren" userFirstLetter="W" />);
    expect(container).toMatchSnapshot();
  });
});
