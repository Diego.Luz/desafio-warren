import React from "react";
import { Logo } from "../Icons/Logo/logo";
import s from "./style.scss";

interface MessageProps {
  text: string;
  isLoading?: boolean;
}

export const Message = ({ text, isLoading }: MessageProps) => {
  return (
    <section className={s.message}>
      <span>
        <Logo className={isLoading ? s.blink : ""} />
      </span>
      <p>{text}</p>
    </section>
  );
};
