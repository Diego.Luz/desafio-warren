export const formatString = (text: string): string =>
  text.replace(/(\^\d[0-9]+)/gi, "") || "";
