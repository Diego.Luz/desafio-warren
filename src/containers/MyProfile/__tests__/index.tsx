import React from "react";
import "@testing-library/jest-dom/extend-expect";
import { render, wait } from "@testing-library/react";
import { MyProfile } from "..";
import { rootStoreMock } from "../../../session/RootSession/__mocks__";
import { Provider } from "mobx-react";

beforeEach(() =>
  rootStoreMock.chatSession.setUserProfile({
    title: "Inovador",
    headline: "",
    coverPictureURL: {
      full: "",
      regular: "",
    },
    sections: [
      {
        title: "",
        type: "",
        content: [
          {
            content: "",
            type: "",
          },
        ],
      },
    ],
  })
);

describe("<MyProfile /> test case", () => {
  test("<MyProfile /> is in the document", () => {
    const { container } = render(<MyProfile />);
    expect(container).toMatchSnapshot();
  });

  test("<MyProfile /> is in the document", () => {
    const { findByText } = render(
      <Provider {...rootStoreMock}>
        <MyProfile />
      </Provider>
    );
    wait(() => {
      expect(findByText("Inovador")).toBe(
        rootStoreMock.chatSession.userProfile.title
      );
    });
  });
});
