import React from "react";
import s from "./style.scss";
import { useSession } from "../../session/RootSession";

export const MyProfile = () => {
  const { chatSession } = useSession();

  return (
    <section className={s.chat}>
      <header
        className={s.header}
        style={{
          backgroundImage: `url(${chatSession.userProfile.coverPictureURL.full})`,
        }}
      >
        <div className={s.title}>
          <h2 data-testid="title">{chatSession.userProfile.title}</h2>
          <p data-testid="headline">{chatSession.userProfile.headline}</p>
        </div>
      </header>
      {chatSession.userProfile.sections.length && (
        <section className={s.content}>
          <div className={s.firstContainer}>
            <div className={s.container}>
              <h2>{chatSession.userProfile.sections[0].title}</h2>
              <p>{chatSession.userProfile.sections[0].content[0].content}</p>
            </div>
          </div>
        </section>
      )}
    </section>
  );
};
