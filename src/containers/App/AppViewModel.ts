export const useAppViewModel = () => {
  const goBack = () => {
    location.href = "https://warren.com.br/app/#/suitability";
  };

  return { goBack } as const;
};
