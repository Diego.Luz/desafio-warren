import React from "react";
import { Router, Switch, Route } from "react-router-dom";
import { Provider, observer } from "mobx-react";
import { rootSession } from "../../session/RootSession";
import { Header } from "../../components/Header";
import { useAppViewModel } from "./AppViewModel";
import { Chat } from "../Chat";
import { MyProfile } from "../MyProfile";
import { SnackbarProvider } from "notistack";
import { CssBaseline } from "@material-ui/core";

export const App = observer(() => {
  const { goBack } = useAppViewModel();
  return (
    <SnackbarProvider preventDuplicate maxSnack={3}>
      <CssBaseline />
      <Provider {...rootSession}>
        <main>
          <Header
            isProfile={
              rootSession.routerSession.location.pathname === "/my-profile"
            }
            goBack={goBack}
          />
          <Router history={rootSession.history}>
            <Switch>
              <Route component={Chat} path="/" exact />
              <Route component={MyProfile} path="/my-profile" exact />
            </Switch>
          </Router>
        </main>
      </Provider>
    </SnackbarProvider>
  );
});
