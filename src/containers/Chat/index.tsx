import React from "react";
import s from "./style.scss";
import { Answer } from "../../components/Answer";
import { Footer } from "../../components/Footer";
import { Message } from "../../components/Message";
import { formatString } from "../../utils/formatString";
import { useChatViewModel } from "./ChatViewModel";
import { InputField } from "./InputField";
import { Button } from "../../components/Button";

export const Chat = () => {
  const {
    fieldName,
    response,
    chat,
    answers,
    isLoading,
    isLastLoading,
    onChangeAnswer,
    handleMessage,
    formatInputValue,
    onButtonClick,
    showProfile,
  } = useChatViewModel();

  return (
    <section className={s.chat}>
      <div className={s.messageContent}>
        {chat.map((message, index) => {
          return (
            <section key={index}>
              {message.type === "M" ? (
                <Message text={formatString(message.text)} />
              ) : (
                <Answer
                  text={message.text}
                  userFirstLetter={
                    Object.assign({ ...answers })["question_name"][0]
                  }
                />
              )}
            </section>
          );
        })}
        {isLoading && response.id !== "final" ? (
          <Message text="" isLoading={isLoading} />
        ) : (
          <Footer isLoading={isLoading} isButton={response.buttons.length > 0}>
            <InputField
              handleMessage={handleMessage}
              inputRender={{
                fieldValue: Object.assign({ ...answers })[fieldName],
                inputs: response.inputs,
                onChangeInput: onChangeAnswer,
                formatInputValue: formatInputValue,
              }}
              buttonRender={{
                buttons: response.buttons,
                onClickOption: (value, title) => onButtonClick(value, title),
              }}
              radioRender={{
                radios: response.radios,
                onClickOption: (value) => console.log(value),
              }}
            />
          </Footer>
        )}
        {response.id === "final" && (
          <Footer isButton={true} isLoading={false}>
            <Button
              isLoading={isLastLoading}
              label="Ver perfil"
              onClick={showProfile}
            />
          </Footer>
        )}
      </div>
    </section>
  );
};
