import {
  FinishResponse,
  MessageRequest,
  QuestionAnswers,
  Responce,
  ProfileResponse,
} from "../../@types/interfaces";

export const useChatModel = () => {
  const message = async (
    params: MessageRequest,
    handleRequest: (response: Responce) => void,
    notificate: (
      message: string,
      status: "default" | "error" | "success" | "warning" | "info"
    ) => void
  ) => {
    await fetch("https://api.dev.oiwarren.com/api/v2/conversation/message", {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify(params),
    })
      .then(async (response) => {
        const data = await response.json();
        handleRequest(data);
      })
      .catch(async (error) => {
        const data = await error.json();
        notificate(data.message, "error");
      });
  };

  const finish = async (
    params: QuestionAnswers,
    handleRequest: (response: FinishResponse) => void,
    notificate: (
      message: string,
      status: "default" | "error" | "success" | "warning" | "info"
    ) => void
  ) => {
    await fetch("https://api.dev.oiwarren.com/api/v2/suitability/finish", {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify({ answers: params }),
    })
      .then(async (response) => {
        const data = await response.json();
        handleRequest(data);
      })
      .catch(async (error) => {
        const data = await error.json();
        notificate(data.message, "error");
      });
  };

  const myProfile = async (
    params: FinishResponse,
    handleRequest: (response: ProfileResponse) => void,
    notificate: (
      message: string,
      status: "default" | "error" | "success" | "warning" | "info"
    ) => void
  ) => {
    await fetch("https://api.dev.oiwarren.com/api/v2/suitability/my-profile", {
      method: "GET",
      headers: {
        "content-type": "application/json; charset=utf-8",
        "access-token": params.accessToken,
      },
    })
      .then(async (response) => {
        const data = await response.json();
        handleRequest(data);
      })
      .catch(async (error) => {
        const data = await error.json();
        notificate(data.message, "error");
      });
  };

  return {
    message,
    finish,
    myProfile,
  } as const;
};
