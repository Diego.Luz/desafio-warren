import { useEffect, useRef, useState } from "react";
import {
  FinishResponse,
  QuestionAnswers,
  Responce,
} from "../../@types/interfaces";
import { useChatModel } from "./ChatModel";
import { numberToMoney } from "../../utils/helpers";
import { useSession } from "../../session/RootSession";
import { useNotification } from "../../hooks/useNotification";

export const useChatViewModel = () => {
  const { chatSession, routerSession } = useSession();
  const notification = useNotification();
  const { message, finish, myProfile } = useChatModel();
  const [answers, setAnswers] = useState<QuestionAnswers[]>([]);
  const [chat, setChat] = useState<{ type: "M" | "A"; text: string }[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isLastLoading, setIsLastLoading] = useState(false);
  const [response, setResponse] = useState<Responce>({
    id: null,
    messages: [],
    buttons: [],
    inputs: [],
    rows: [],
    radios: [],
    checkbox: [],
    responses: [],
  });

  useEffect(() => {
    handleMessage();
  }, []);

  const onChangeAnswer = (value: string | number) => {
    if (response.id && response.id !== "final") {
      setAnswers({
        ...answers,
        [response.id]: value,
      });
    }
  };

  const onButtonClick = (value: string, title: string) => {
    if (response.id && response.id !== "final") {
      setAnswers({ ...answers, [response.id]: value });
      setChat([
        ...chat,
        {
          type: "A",
          text: title,
        },
      ]);
    }
  };

  const handleMessage = async () => {
    if (response.inputs.length) {
      if (response.responses.length) {
        setChat([
          ...chat,
          {
            type: "A",
            text: response.responses[0].replace(
              "{{answers.question_name}}",
              Object.assign({ ...answers })["question_name"]
            ),
          },
        ]);
      } else if (response.id) {
        setChat([
          ...chat,
          {
            type: "A",
            text:
              response.id === "question_income"
                ? numberToMoney(
                    Object.assign({ ...answers })[response.id as string]
                  )
                : Object.assign({ ...answers })[response.id as string],
          },
        ]);
      }
    }

    setIsLoading(true);

    await message(
      {
        context: "suitability",
        answers: Object.assign({ ...answers }),
        id: response.id,
      },
      async (data) => {
        setResponse(data);
        setChat((chatData) => [
          ...chatData,
          ...data.messages.map((message) => ({
            type: "M" as "M" | "A",
            text: message.value,
          })),
        ]);
        setAnswers({
          ...answers,
          [data.id as string]:
            data.inputs.length && data.inputs[0].type === "number" ? 0 : "",
        });
        if (data.id === "final") {
          setIsLastLoading(true);
          await getFinalResponse();
        }
      },
      notification
    );

    setIsLoading(false);
  };

  const getFinalResponse = async () => {
    await finish(
      Object.assign({ ...answers }),
      async (response) => {
        await checkUserProfile(response);
      },
      notification
    );
  };

  const checkUserProfile = async (response: FinishResponse) => {
    await myProfile(
      response,
      (data) => {
        chatSession.setUserProfile(data);
      },
      notification
    ).finally(() => setIsLastLoading(false));
  };

  const formatInputValue = (mask: string, value: string | number) => {
    switch (mask) {
      case "currency":
        return numberToMoney(Number(value));

      case "integer":
        return value === 0 || value === "" ? "" : value;

      default:
        return value;
    }
  };

  const showProfile = () => {
    routerSession.push("/my-profile");
  };

  return {
    fieldName:
      response.id && response.id !== "final" ? response.id : "question_name",
    response,
    chat,
    answers,
    isLoading,
    isLastLoading,
    onChangeAnswer,
    handleMessage,
    formatInputValue,
    onButtonClick,
    showProfile,
  } as const;
};
