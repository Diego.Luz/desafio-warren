import React from "react";
import s from "./style.scss";
import { InputType, UserResponseType } from "../../../@types/interfaces";
import { Button } from "../../../components/Button";
import { justNumbers } from "../../../utils/helpers";

interface InputFieldProps {
  handleMessage(): void;
  inputRender: {
    inputs: InputType[];
    fieldValue: string | number;
    onChangeInput(value: string): void;
    formatInputValue(mask: string, value: string | number): string | number;
  };
  buttonRender: {
    buttons: UserResponseType[];
    onClickOption(value: string, title: string): void;
  };
  radioRender: {
    radios: UserResponseType[];
    onClickOption(value: string): void;
  };
}

export const InputField = ({
  handleMessage,
  inputRender,
  buttonRender,
  radioRender,
}: InputFieldProps) => {
  return (
    <form
      className={
        buttonRender.buttons.length || radioRender.radios.length
          ? s.formForButton
          : s.form
      }
      onSubmit={(e) => {
        e.preventDefault();
        handleMessage();
      }}
    >
      {inputRender.inputs.length ? (
        <>
          {inputRender.inputs.map((item, index) => {
            return (
              <input
                key={`${index}${item.mask}`}
                type="text"
                placeholder={item.mask === "name" ? "Digite seu nome" : ""}
                value={inputRender.formatInputValue(
                  item.mask,
                  inputRender.fieldValue
                )}
                onChange={(e) =>
                  inputRender.onChangeInput(
                    item.mask === "currency"
                      ? justNumbers(e.target.value)
                      : e.target.value
                  )
                }
              />
            );
          })}
          <button
            className={
              !inputRender.fieldValue?.toString().length ? s.buttonDisabled : ""
            }
            onClick={(e) => {
              e.preventDefault();
              if (inputRender.fieldValue?.toString().length) {
                handleMessage();
              }
            }}
          >
            Ok
          </button>
        </>
      ) : null}
      {buttonRender.buttons.length
        ? buttonRender.buttons.map((button, index) => {
            return (
              <Button
                key={`${index}-${button.label.title}`}
                label={button.label.title}
                onClick={() =>
                  buttonRender.onClickOption(button.value, button.label.title)
                }
              />
            );
          })
        : null}
      {radioRender.radios.length
        ? radioRender.radios.map((button, index) => {
            return (
              <Button
                key={`${index}-${button.label.title}`}
                label={button.label.title}
                onClick={() => radioRender.onClickOption(button.value)}
              />
            );
          })
        : null}
    </form>
  );
};
