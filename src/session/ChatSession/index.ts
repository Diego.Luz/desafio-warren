import { observable, action } from "mobx";
import { ProfileResponse } from "../../@types/interfaces";

export class ChatSession {
  @observable
  userProfile: ProfileResponse = {
    title: "",
    headline: "",
    coverPictureURL: {
      full: "",
      regular: "",
    },
    sections: [
      {
        title: "",
        type: "",
        content: [
          {
            content: "",
            type: "",
          },
        ],
      },
    ],
  };

  @action
  public setUserProfile = (profile: ProfileResponse) => {
    this.userProfile = profile;
  };
}
