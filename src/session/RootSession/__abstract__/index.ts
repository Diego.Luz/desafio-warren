import { RouterStore as RouterSession } from "mobx-react-router";
import { History } from "history";

export abstract class AbstractRootSession {
  public abstract routerSession: RouterSession;
  public abstract history: History;
}
