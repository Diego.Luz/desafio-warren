import {
  syncHistoryWithStore,
  RouterStore as RouterSession,
} from "mobx-react-router";
import { History, createBrowserHistory } from "history";
import { createContext, useContext } from "react";
import { AbstractRootSession } from "../__abstract__";
import { ChatSessionMocks } from "../../ChatSession/__mocks__";

export class RootStoreMock extends AbstractRootSession {
  public routerSession: RouterSession;
  public chatSession: ChatSessionMocks;
  public history: History;

  public constructor() {
    super();
    const browerHistory = createBrowserHistory();
    this.routerSession = new RouterSession();
    this.chatSession = new ChatSessionMocks();
    this.history = syncHistoryWithStore(browerHistory, this.routerSession);

    return {
      routerSession: this.routerSession,
      chatSession: this.chatSession,
      history: this.history,
    };
  }
}

export const rootStoreMock = new RootStoreMock();
export const rootStoreContextMock = createContext(rootStoreMock);
