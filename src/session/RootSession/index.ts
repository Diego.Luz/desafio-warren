import * as session from "../index";
import { syncHistoryWithStore } from "mobx-react-router";
import { History, createBrowserHistory } from "history";
import { createContext, useContext } from "react";
import { AbstractRootSession } from "./__abstract__";
import { rootStoreContextMock, RootStoreMock } from "./__mocks__";

export class RootSession extends AbstractRootSession {
  public routerSession: session.RouterSession;
  public chatSession: session.ChatSession;
  public history: History;

  public constructor() {
    super();
    const browerHistory = createBrowserHistory();
    this.routerSession = new session.RouterSession();
    this.chatSession = new session.ChatSession();
    this.history = syncHistoryWithStore(browerHistory, this.routerSession);

    return {
      routerSession: this.routerSession,
      chatSession: this.chatSession,
      history: this.history,
    };
  }
}

export const rootSession = new RootSession();
export const rootSessionContext = createContext(rootSession);
export let useSession = (): session.RootSession | RootStoreMock => {
  return useContext(rootSessionContext);
};
if (process.env.UNIT_TEST) {
  useSession = () => {
    return useContext(rootStoreContextMock);
  };
}
